FROM docker.io/opensuse/tumbleweed

USER root

RUN zypper --non-interactive dup && \
    zypper --non-interactive in -t pattern devel_basis && \
    zypper --non-interactive in android-tools \
    autoconf213 \
    bc \
    bison \
    bzip2 \
    ccache \
    clang \
    curl \
    flex \
    gawk \
    gpg2 \
    gperf \
    gcc-c++ \
    git \
    git-lfs \
    glibc-devel \
    ImageMagick \
    java-11-openjdk \
    liblz4-1 \
    libncurses5 \
    libncurses6 \
    libpopt0 \
    libressl-devel \
    libstdc++6\
    libxml2-tools \
    libxslt1 \
    libXrandr2 \
    lzip \
    lzop \
    kernel-devel \
    maven \
    make \
    Mesa-libGL1 \
    Mesa-libGL-devel \
    mokutil \
    ncurses5-devel \
    ncurses-devel \
    openssl \
    patch \
    perl-Digest-SHA1 \
    readline-devel \
    sha3sum \
    squashfs \
    wget \
    zip \
    git-repo \
    rsync \
    unzip \
    python311

COPY fox.py /bin/fox
COPY *.py /bin/.
RUN chmod +x /bin/fox

RUN useradd -U -m -u 1000 fox
USER fox
CMD ["/bin/fox"]