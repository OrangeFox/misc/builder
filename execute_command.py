import subprocess
import select


def execute_command(command: str, allow_fail: bool = False) -> int:
    print("Execute:" + command)

    process = subprocess.Popen(
        command,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        bufsize=1,  # Line buffered
        universal_newlines=True
    )

    while True:
        # Check if the process has terminated
        if process.poll() is not None:
            break

        # Check for available data to read
        readable, _, _ = select.select([process.stdout, process.stderr], [], [])

        # Read and print the available data
        for stream in readable:
            if stream == process.stdout:
                line = process.stdout.readline()
                if line:
                    print(line, end='', flush=True)  # Print stdout
            elif stream == process.stderr:
                line = process.stderr.readline()
                if line:
                    print("!!!:", line, end='', flush=True)  # Print stderr

    # Read remaining output
    remaining_stdout, remaining_stderr = process.communicate()
    if remaining_stdout:
        print(remaining_stdout, end='', flush=True)
    if remaining_stderr:
        print("!!!:", remaining_stderr, end='', flush=True)

    if allow_fail and process.returncode != 0:
        print('!!! Exit code is not 0! Exiting immediately!')
        exit(1)

    return process.returncode
