#!/bin/python3
import argparse
import shlex
from execute_command import execute_command

parser = argparse.ArgumentParser(
    description="OrangeFox build",
    prog='roomfox',
)
parser.add_argument('--treeurl')
parser.add_argument(
    '--imagetype',
    default='recoveryimage',
    choices=['recoveryimage', 'bootimage', 'vendorbootimage']
)
parser.add_argument('--buildtype', default='userdebug')
parser.add_argument('--buildercpsources')
parser.add_argument('--sources', default='/fox')
parser.add_argument('codename')

args = parser.parse_args()

if args.buildercpsources:
    print('Ensuring the sources directory is clear...')
    execute_command(f'rm -rf {shlex.quote(args.sources)}/*')
    print('Copying the temporary OrangeFox sources...')
    execute_command(f'cp -r {shlex.quote(args.buildercpsources)} {shlex.quote(args.sources)}')

if args.treeurl:
    if '@' not in args.treeurl:
        print('--treeurl must contain @ separators between url, path and branch')
        exit(2)

    tree_url, tree_path, tree_branch = args.treeurl.split('@')

    print('Cloning device tree...')
    execute_command(f'git clone {shlex.quote(tree_url)} {shlex.quote(tree_path)} -b {shlex.quote(tree_branch)}')

print('Starting build process...')

escaped_sources = shlex.quote(args.sources)
escaped_codename = shlex.quote(args.codename)
escaped_buildtype = shlex.quote(args.buildtype)
escaped_imagetype = shlex.quote(args.imagetype)

execute_command(f"""bash -c "
cd {escaped_sources};
source build/envsetup.sh;
lunch twrp_{escaped_codename}-{escaped_buildtype};
mka adbd {escaped_imagetype}"
""")
